//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/	 	/*Update Interval*/	/*Update Signal*/
  {"",     "memory",       1,        2},
  {"",     "disk-space",   3600,     3},
  {"",     "cpu",          30,       4},
  {"",     "temp",         15,       5},
  {"",     "gpu",          30,       6},
  {"",     "pacupdate",    3600,     7},
  {"",     "volume",       0,        8},
  {"",     "network",      3,        9},
  {"",     "corona",       3600,    10},
  {"",     "kernel",       0,       11},
  {"",     "upt",          60,      12},
  {"",     "moonphase",    18000,   13},
  {"",     "forecast",     3600,    14},
  {"",     "clock",        1,        1},

};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim = ' ';
